#include <pjsr/ColorSpace.jsh>
#include <pjsr/DataType.jsh>
#include <pjsr/UndoFlag.jsh>

#include "WarpImage.js"
#include "WCSmetadata.jsh"

function SkyMapAnnotation() {

   /******************************* PARAMETERS ********************************/

   // target focal length of the SkyMap (in mm)
   let targetFL = 60;

   // TRUE if north points UP, false if north points DOWN
   let northUP = true;

   // percentage of white border's thickness to be applied before warping the
   // image into the SkyMap
   let borderThickness = 0.05;

   // dimensions of the SkyMap in pixels
   let width = 6000;
   let height = 4000;

   /***************************************************************************/

   /* Get a float value from the astrometric solution */
   this.getFloat = (AS, key) => {
      for (let i=0; i < AS.length; i++) {
         if (AS[i].name == key)
            return parseFloat(AS[i].value);
      }
      return NaN;
   }

   /* Update an astrometric value */
   this.update = (AS, name, value) => {
      for (let i=0; i < AS.length; i++) {
         if (AS[i].name == name) {
            AS[i].value = "" + value;
            return;
         }
      }
   }

   /* Clone an image assigning the given ID */
   this.clone = (window, id) => {

      let newImg = new ImageWindow(
         window.mainView.image.width,
         window.mainView.image.height,
         window.mainView.image.numberOfChannels,
         window.bitsPerSample,
         window.isFloatSample,
         window.mainView.image.colorSpace != ColorSpace_Gray,
         id
         );

      newImg.mainView.beginProcess(UndoFlag_NoSwapFile);
      newImg.mainView.image.assign( window.mainView.image );
      newImg.mainView.endProcess();

      return newImg;
   }

   /* Adds a white border around the image */
   this.addBorder = (window, thickness) => {
      let min = "" + thickness;
      let max = "" + (1 - thickness);

      var P = new PixelMath;
      P.expression = "iif(X()<" + min + " || X() > " + max + " || Y() < " + min + " || Y() > " + max + ", 1, $T)";
      P.expression1 = "";
      P.expression2 = "";
      P.expression3 = "";
      P.useSingleExpression = true;
      P.symbols = "";
      P.clearImageCacheAndExit = false;
      P.cacheGeneratedImages = false;
      P.generateOutput = true;
      P.singleThreaded = false;
      P.optimization = true;
      P.use64BitWorkingImage = false;
      P.rescale = false;
      P.rescaleLower = 0;
      P.rescaleUpper = 1;
      P.truncate = true;
      P.truncateLower = 0;
      P.truncateUpper = 1;
      P.createNewImage = false;
      P.showNewImage = true;
      P.newImageId = "";
      P.newImageWidth = 0;
      P.newImageHeight = 0;
      P.newImageAlpha = false;
      P.newImageColorSpace = PixelMath.prototype.SameAsTarget;
      P.newImageSampleFormat = PixelMath.prototype.SameAsTarget;

      P.executeOn(window.mainView);
   }

   /* Safely get the focal length from the astrometric
    * solution.
    */
   this.getFocalLength = (AS) => {
      let fl = this.getFloat(AS, "FOCALLEN");
      if (fl == NaN) {
         console.warningln("Missing keyword FOCALLEN from astrometric solution. Use a scale of 5x");
         return FL * 5;
      }
      return fl;
   }

   //
   //
   //

   // get the current active window
   let sourceWindow = ImageWindow.activeWindow;

   // an active window must exist
   if (sourceWindow == null) {
      console.errorln("An active image must be set.");
      return;
   }

   // the active window must have a valid astrometric solution
   let AS = sourceWindow.astrometricSolution();
   if (AS == null || AS.length == 0) {
      console.criticalln("The active image must have an astrometric solution.");
      return;
   }
   AS = AS[0];

   // print the new solution to the console
   console.writeln(sourceWindow.astrometricSolutionSummary());

   // craete the imagfe for annotation
   let SkyMapImage = new ImageWindow(
      width,
      height,
      3,       // number of channels
      32,      // bit per pixel
      true,    // true = float
      true,    // true = RGB
      "FOVImage");

   // determine the scale factor corresponding to the desired focal length
   let originalFL = this.getFocalLength(AS);
   let scale = originalFL / targetFL;

   // update the astrimetric solution orienting the image with Notrh in top
   // direciton and using Zenithal Equal Area as projection method
   let CD1_1 = this.getFloat(AS, "CD1_1") * scale;
   let CD1_2 = this.getFloat(AS, "CD1_2") * scale;
   let CD2_1 = this.getFloat(AS, "CD2_1") * scale;
   let CD2_2 = this.getFloat(AS, "CD2_2") * scale;
   let CD1 = Math.sqrt(CD1_1*CD1_1 + CD2_1*CD2_1);
   let CD2 = Math.sqrt(CD1_2*CD1_2 + CD2_2*CD2_2);

   // center and derotate
   this.update(AS, "CD1_1", -CD1);
   this.update(AS, "CD1_2", 0);
   this.update(AS, "CD2_1", 0);
   this.update(AS, "CD2_2", CD2 * (northUP ? 1 : -1));
   this.update(AS, "CRPIX1", width / 2);
   this.update(AS, "CRPIX2", height / 2);

   //this.update(AS, "CROTA1", 0);
   //this.update(AS, "CROTA2", 0);
   this.update(AS, "PV1_1", 0);
   this.update(AS, "PV1_2", 90);

   this.update(AS, "CTYPE1", "RA---ZEA");
   this.update(AS, "CTYPE2", "DEC--ZEA");

   // store the solution to accomodate the annotation script
   SkyMapImage.setAstrometricSolution(AS);

   // print the new solution to the console
   console.noteln(SkyMapImage.astrometricSolutionSummary());

   // create the bordered version
   let clonedBordered = this.clone(sourceWindow, "BORDERED");
   this.addBorder(clonedBordered, borderThickness);
   clonedBordered.copyAstrometricSolution(sourceWindow);
   clonedBordered.updateAstrometryMetadata();

   // Project bordered image into the target image
   let metadataTarget = null
   try {
      metadataTarget = new ImageMetadata();
      metadataTarget.ExtractMetadata(SkyMapImage);
   } catch(exception) {
      console.writeln(exception);
      return;
   }

   var warp = new WarpImage();
   let resWindow = warp.AdaptImageHQ(metadataTarget, clonedBordered);
   resWindow.mainView.id = "Annotated"
   resWindow.show();

   // release the working images
   SkyMapImage.forceClose();
   clonedBordered.forceClose();
}


console.show();
SkyMapAnnotation();
